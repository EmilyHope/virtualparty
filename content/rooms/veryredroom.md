---
title: "Veryredroom"
date: 2020-03-27T23:56:24Z
draft: false
---

Location [Basement]
Exits [Basement_Hall][Garden]

This room is carpeted on almost every surface. As you walk in you feel your feet sink slightly into the carpet, a layer of sponge.  There are hammocks and a strong shelf around room at the top of the room.  You can see it would be easy to climb up and that the shelves are wide enough to lie on. A lit wood burner warms the room. 

![Red Room](/redfroom.jpg)

Now please check that you have done the necessary technical things here

And if you want to listen to the music in the Red Room CLICK HERE 

[Here is the Virtual Party Red Room](https://unzai.com/stream/meet.html)

To see a video about the room click here

{{< youtube WAexfmw2zS0>}}
