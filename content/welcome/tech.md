---
title: "Tech"
date: 2020-03-28T02:41:08Z
draft: false
---

So. You wanna connect. We want there to b many ways to connect and we want it to be easy.  As yet it is not as easy or well designed as we would like. yet..


This wee 'eco' system is held together with bits of string. There can and will be improvements. But it might just work. And if it does, we will improve it.  We want this to be a permanet place to hang out over the next few weeks. Or at least, a place to gather at the weekend or whatever.

The idea is that you can join in as much or as little as you liek. you could just listen to the music stream and chat in a text channel for instance. : )  We are all shy and that is fine.

TEXT CHAT
---------
The text chat system in jitsi is not as lovely as we would like.  We want to build a better one. We woudl love there to be a decent chat udnerneath the video room adn for there to be decent ways to for us to interact between both.

MUSIC
-----
Its a bit like a silent disco.

To listen to the music you need VLC https://www.videolan.org/. Then open a network stream and paste in the rooms music stream. e.g.  https://unzai.com/party  We are using our own music/video stream because the quality is higher than in any  conference call software we have found and we can more easily work with DJ's.  

Experiments suggest that you want to have the music lower volume than the volume of the room.

PARTY ROOM
----------
To join a room you *must* have headphones. Otherwise you could cause feedback as your microphone picks up its own sounds from the room - *painful*. IF this starts **mute your mic** and then your speakers ASAP.  This is harder to do if you are on a phone, so best to just hangup ASAP.
However we have been testing this a lot and generally it only happens because we are running multiple machines, multiple mics, cams and phones. 

The rooms can supposedly take 200 people. But realisticly any room will begin to get overwhelmed with more than 6.
If this happens - huzzagh! the party has been a success. You are welcome to create new rooms and let us know, and we could try and update this website.

Try to enter the room quietly and stay quiet unless you are chatting.  use hand signals, flappign etc.  Only one person can talk at a time in these rooms. 

Use Chrome, or Chromium.  Do not have any privacy/cookie/script/blocking plugins.  When you first connect, it will tell you that you need to install the 365/google calednar plugin. you don't. not for this *party* : )

About Security and Safety.
-------------------------
We hate corporate websites, social media, profiling, adverts. We love security, DIY, Open Source Software, privacy.  

and of course, all this security and safety is merely an  illusion! However there are some great arguements for free software, such as the  Public Money, Public Code from the EFSF.

THANK YOU for reading this far.... now to the [Party Rooms](/rooms) 
